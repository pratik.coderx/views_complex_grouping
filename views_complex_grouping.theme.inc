<?php

/**
 * @file
 * Contains Preprocessor for views_view_complex_grouping_level.html.twig.
 */

/**
 * Prepare variables for views complex grouping template.
 */
function template_preprocess_views_view_complex_grouping_level(&$variables) {
  $sdfs = "";
  $variables['content'] = $variables['view']
    ->style_plugin
    ->renderGroupingSets($variables['rows']);
}
