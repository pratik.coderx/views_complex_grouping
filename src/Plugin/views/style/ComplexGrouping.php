<?php

namespace Drupal\views_complex_grouping\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Class ComplexGrouping.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "complex_grouping",
 *   title = @Translation("Complex Grouping"),
 *   help = @Translation("Limit the number of rows under each grouping field"),
 *   theme = "views_view_complex_grouping_leave",
 *   display_types = { "normal" }
 * )
 */
class ComplexGrouping extends StylePluginBase {

  /**
   * Whether or not this style uses a row plugin.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support grouping of rows.
   *
   * @var bool
   */
  protected $usesGrouping = TRUE;

  /**
   * Denotes whether the plugin has an additional options form.
   *
   * @var bool
   */
  protected $usesOptions = TRUE;

  /**
   * Does the style plugin for itself support to add fields to its output.
   *
   * This option only makes sense on style plugins without row plugins, like
   * for example table.
   *
   * @var bool
   */
  protected $usesFields = TRUE;

  /**
   * Name of the template used render.
   *
   * @var string
   */
  protected $complexGroupingTheme = 'views_view_complex_grouping_level';

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    parent::buildOptionsForm($form, $form_state);
    $field_labels = $this->displayHandler->getFieldLabels(TRUE);

    foreach ($form['grouping'] as $index => $info) {
      $form['grouping'][$index]['complex_grouping'] = [
        '#type' => 'fieldset',
        '#title' => t('Limit and extra fields for grouping field No. @num', ['@num' => $index + 1]),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      ];
      $form['grouping'][$index]['complex_grouping']['grouping_fields'] = [
        '#type' => 'select',
        '#multiple' => TRUE,
        '#title' => t('Selected'),
        '#options' => $field_labels,
        '#default_value' => $this->getGroupingDefaultValues($index, 'grouping_fields'),
        '#description' => t('Select which fields will be displayed alongside the field No. @num', ['@num' => $index + 1]),
      ];
      $form['grouping'][$index]['complex_grouping']['grouping_limit'] = [
        '#type' => 'textfield',
        '#title' => t('Items to display:'),
        '#default_value' => $this->getGroupingDefaultValues($index, 'grouping_limit'),
        '#size' => 6,
        '#element_validate' => [[get_called_class(), 'complexGroupingValidate']],
        '#description' => t('The number of rows to show under the field Nr. @num. Leave 0 to show all of them.', ['@num' => $index + 1]),
      ];
      $form['grouping'][$index]['complex_grouping']['grouping_offset'] = [
        '#type' => 'textfield',
        '#title' => t('Offset:'),
        '#default_value' => $this->getGroupingDefaultValues($index, 'grouping_offset'),
        '#size' => 6,
        '#element_validate' => [[get_called_class(), 'complexGroupingValidate']],
        '#description' => t('The row to start on.'),
      ];
    }

  }

  /**
   * Get default value for complex grouping options.
   */
  private function getGroupingDefaultValues($index, $key) {

    $default_option = NULL;
    $options = $this->options['grouping'];
    switch ($key) {
      case 'grouping_limit':
        $default_value = 0;
        break;

      case 'grouping_offset':
        $default_value = 0;
        break;

      case 'grouping_fields':
      default:
        $default_value = NULL;
        break;
    }

    $default_option = isset($options[$index]) ? $options[$index]['complex_grouping'][$key]?: $default_value : $default_value;
    return $default_option;
  }

  /**
   * Group records as needed for rendering.
   */
  public function renderGrouping($records, $groupings = [], $group_rendered = NULL) {
    // This is for backward compatibility, when $groupings was a string
    // containing the ID of a single field.
    if (is_string($groupings)) {
      $rendered = $group_rendered === NULL ? TRUE : $group_rendered;
      $groupings = [['field' => $groupings, 'rendered' => $rendered]];
    }
    // Make sure fields are rendered.
    $this->renderFields($this->view->result);
    $sets = [];
    if ($groupings) {
      foreach ($records as $index => $row) {
        // Iterate through configured grouping fields to determine the
        // hierarchically positioned set where the current row belongs to.
        // While iterating, parent groups, that do not exist yet, are added.
        $set = &$sets;

        foreach ($groupings as $level => $info) {
          $field = $info['field'];
          $rendered = isset($info['rendered']) ? $info['rendered'] : $group_rendered;
          $rendered_strip = isset($info['rendered_strip']) ? $info['rendered_strip'] : FALSE;
          $grouping = '';
          $group_content = '';

          // Group on the rendered version of the field, not the raw.  That way,
          // we can control any special formatting of the grouping field through
          // the admin or theme layer or anywhere else we'd like.
          if (isset($this->view->field[$field])) {

            $group_content = $this->getField($index, $field);
            if ($this->view->field[$field]->options['label']) {
              $group_content = $this->view->field[$field]->options['label'] . ': ' . $group_content;
            }
            if ($rendered) {
              $grouping = (string) $group_content;
              if ($rendered_strip) {
                $group_content = $grouping = strip_tags(htmlspecialchars_decode($group_content));
              }
            }
            else {
              $grouping = $this->getFieldValue($index, $field);
              // Not all field handlers return a scalar value,
              // e.g. views_handler_field_field.
              if (!is_scalar($grouping)) {
                $grouping = hash('sha256', serialize($grouping));
              }
            }
          }

          // Create the group if it does not exist yet.
          if (empty($set[$grouping])) {
            $set[$grouping]['group'] = $group_content;
            $set[$grouping]['level'] = $level;
            $set[$grouping]['rows'] = [];
            $set[$grouping]['fields'] = [];

            // Add selected fields for this level.
            foreach ($this->options['grouping'][$level]['complex_grouping']['grouping_fields'] as $field) {
              $set[$grouping]['fields'][$field] = $this->rendered_fields[$index][$field];
            }

          }
          // Move the set reference into the row set of the group
          // we just determined.
          $set = &$set[$grouping]['rows'];
        }
        // Add the row to the hierarchically positioned row set
        // we just determined.
        $set[$index] = $row;
      }
    }
    else {
      // If this parameter isn't explicitly set, modify the output to be fully
      // backward compatible to code before Views 7.x-3.0-rc2.
      // @TODO Remove this as soon as possible e.g. October 2020
      if ($group_rendered === NULL) {
        $old_style_sets = [];
        foreach ($sets as $group) {
          $old_style_sets[$group['group']] = $group['rows'];
        }
        $sets = $old_style_sets;
      }
    }

    // Apply the offset and limit.
    array_walk($sets, [$this, 'complexGroupingRecursiveLimit']);

    return $sets;
  }

  /**
   * Render the grouping sets.
   */
  public function renderGroupingSets($sets) {
    $output = [];
    $branch = 0;
    $theme_functions = $this->view->buildThemeFunctions($this->complexGroupingTheme);
    foreach ($sets as $set) {
      $branch++;
      $level = isset($set['level']) ? $set['level'] : 0;

      $row = reset($set['rows']);
      // Render as a grouping set.
      if (is_array($row) && isset($row['group'])) {
        $single_output = [
          '#theme' => $theme_functions,
          '#view' => $this->view,
          '#grouping' => $this->options['grouping'][$level],
          '#grouping_branch' => $branch,
          '#rows' => $set['rows'],
          '#fields' => $set['fields'],
        ];
      }
      // Render as a record set.
      else {
        if ($this->usesRowPlugin()) {
          foreach ($set['rows'] as $index => $row) {
            $this->view->row_index = $index;
            $set['rows'][$index] = $this->view->rowPlugin->render($row);
          }
        }

        $single_output = $this->renderRowGroup($set['rows']);
        $single_output['#grouping'] = $this->options['grouping'][$level];
        $single_output['#grouping_branch'] = $branch;
        $single_output['#fields'] = $set['fields'];
      }

      $single_output['#grouping_level'] = $level + 1;
      $single_output['#title'] = $set['group'];
      $output[] = $single_output;
    }
    unset($this->view->row_index);
    return $output;
  }

  /**
   * Recursively limits the number of rows in nested groups.
   */
  protected function complexGroupingRecursiveLimit(array &$group_data, $key = NULL, $level = 1) {
    $settings = $this->complexGroupingSettings($level - 1);
    $settings['grouping_limit'] = ($settings['grouping_limit'] != 0) ? $settings['grouping_limit'] : NULL;
    $settings['grouping_offset'] = (isset($settings['grouping_offset'])) ? $settings['grouping_offset'] : 0;

    // Slice up the rows according to the offset and limit.
    $group_data['rows'] = array_slice($group_data['rows'], $settings['grouping_offset'], $settings['grouping_limit'], TRUE);

    // For each row, if it appears to be another grouping, recurse again.
    foreach ($group_data['rows'] as &$data) {
      if (is_array($data) && isset($data['group']) && isset($data['rows'])) {
        $this->complexGroupingRecursiveLimit($data, NULL, $level + 1);
      }
    }
  }

  /**
   * Validate Complex Grouping form options.
   */
  public static function complexGroupingValidate($element, FormStateInterface $form_state, $form) {
    if (!is_numeric($element['#value'])) {
      $form_state->setError($element,
        t('%element must be numeric.',
          ['%element' => $element['#title']]
        ));
    }

    // Checking for negative val  ues.
    if ($element['#value'] < 0) {
      $form_state->setError($element,
        t('%element cannot be negative.',
          ['%element' => $element['#title']]
        ));
    }
  }

  /**
   * Helper function to retrieve settings for grouping limit.
   *
   * @param int $index
   *   The grouping level to fetch settings for.
   *
   * @return array
   *   Settings for this grouping level.
   */
  protected function complexGroupingSettings($index) {
    if ($this->options['grouping'][$index] && $this->options['grouping'][$index]['complex_grouping']) {
      return $this->options['grouping'][$index]['complex_grouping'];
    }
    else {
      return ['grouping_limit' => 0, 'grouping_offset' => 0];
    }
  }

}
